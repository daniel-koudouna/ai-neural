require_relative './functions.rb'

# A neuron in the neural network which has connections
# to other neurons in the network
class Neuron
  attr_accessor :value, :inputs, :outputs, :function

  attr_accessor :error
  
  def initialize(function = Sigmoid)
    @inputs = []
    @outputs = []
    @function = function
  end
  
  def clear_value
    @value = nil
  end

  def has_value?
    !@value.nil?
  end

  # The neuron will output a value only if all its inputs can provide
  # a weighted value
  def accept
    if (@inputs.all? {|c| c.has_value? })
      @value = @function.f (@inputs.map{|c| c.value }.reduce{|a,b| a + b})
    end
  end

  # Obtains the derivative of the function associated with the neuron
  # on the last value that the neuron received
  def gradient
    @function.df(@value)
  end

  # Connect this neuron with another with a random weight. Used when
  # constructing a new network
  def >(other)
    c = Connection.new(self,other)
    other
  end

  # Connect this neuron with another with a specified weight. Used when
  # constructing the network from .json
  def connect(other, weight)
    c = Connection.new(self,other,weight)
    other
  end
end

# The input neuron is a special neuron which can accept a value immediately
class InputNeuron < Neuron
  def accept(val)
    @value = val
  end
end

# A connection between two neurons in the network
class Connection
  attr_accessor :from, :to, :weight, :next_weight, :weight_diff, :value

  attr_accessor :error

  # The connection's weight is initialized to a random number from -1 to 1
  # if not supplied
  def initialize(from,to,weight= (2*rand - 1) )
    @from = from
    @to = to
    @from.outputs << self
    @to.inputs << self
    @weight = weight
    @weight_diff = 0
  end

  def clear_value
    @value = nil
  end

  def has_value?
    !@value.nil?
  end

  # The connection will output a value only if the input neuron has a value
  def accept
    if (from.has_value?)
      @value = from.value*@weight
    end
  end
end
