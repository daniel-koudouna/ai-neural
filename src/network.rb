require_relative './neuron.rb'
require_relative './functions.rb'
require_relative './network_settings.rb'
require 'json'

# The abstraction of the neural network. The network class does
# not depend on layers or the need to be fully connected, which means
# it can create networks which do not meet the specification.
class Network
  attr_accessor :inputs, :outputs, :neurons, :connections
 
  def initialize(inputs, outputs)
    @inputs = inputs
    @outputs = outputs
    @neurons = []
    @connections = []

    neuron_list = inputs.dup

    # Register all neurons and connections in the network
    while (neuron_list.size != 0)   
      neuron = neuron_list.pop

      @neurons << neuron unless @neurons.include? neuron
      neuron.outputs.each do |out|
        @connections << out unless @connections.include? out
        neuron_list << out.to unless @neurons.include? out.to
      end
    end
  end

  # Build a neural network from a set of layers and associated
  # functions for each layer
  def self.build(input_neurons, *layers)
    inputs = []
    input_neurons.times{|n| inputs << InputNeuron.new }
    current_layer = inputs
    
    layers.each do |size, func|
      next_layer = []
      size.times{|n| next_layer << Neuron.new(func) }
      current_layer.each do |from|
        next_layer.each{|to| from > to }
      end
      current_layer = next_layer
    end

    Network.new(inputs, current_layer)
    
  end

  # Feed forward an input vector through the network
  def accept(inputs)
    return nil if inputs.size != @inputs.size

    @neurons.each{|n| n.clear_value }
    @connections.each{|c| c.clear_value }
    
    @inputs.zip(inputs).each{|input,val| input.accept val }

    has_result = @outputs.all? {|out| out.has_value? }

    while( !has_result )
      @connections.each {|c| c.accept unless c.has_value? }
      @neurons.each {|n| n.accept unless n.has_value? }
      has_result = @outputs.all? {|out| out.has_value? }
    end

    @outputs.map{|o| o.value }
  end

  # Back propagates through the network. Since the network
  # does not depend on layers, a queue is used to yield elements
  # to a block at the appropriate order. Each element yielded is
  # guaranteed to be processable (i.e all elements forward of it
  # in the network have already been yielded)
  def back_propagate()
    all_neurons = []
    all_connections = []

    current_neurons = []
    current_connections = []

    @outputs.each{|out| out.inputs.each{|input| current_connections << input}}

    while(current_neurons.any? || current_connections.any?)
      # Prioritize the neuron queue to calculate their total error
      if (current_neurons.any?)
        out = current_neurons.shift

        # The output neurons must be evaluated before the target neuron
        outputs_mapped = out.outputs.all? {|c| c.to.error != nil }
        if (!outputs_mapped)
          current_neurons << out
          next
        end
        
        all_neurons << out
        yield out

        out.inputs.each do |c|
          current_connections << c unless (current_connections.include?(c) || all_connections.include?(c))
        end

        next
      end

      if (current_connections.any?)
        con = current_connections.shift

        # If the input neuron of the connection has not been evaluated yet, add it to the queue
        current_neurons << con.from unless (current_neurons.include?(con.from) || all_neurons.include?(con.from))

        # If the input neuron of the connection is still queued, requeue the connection
        if (current_neurons.include?(con.from))
          current_connections << con
          next
        end

        all_connections << con
        yield con
      end
    end
  end
  
  def train(input_vector, ex_output_vector)
    @neurons.each{|n| n.error = nil }
    @connections.each{|c| c.error = nil }
    
    output_vector = accept(input_vector)
    return if output_vector == nil

    mse = 0.0
    
    # Calculate error on output layer
    @outputs.zip(output_vector, ex_output_vector).each do |out, real, expected|
      out.error = (expected - real)
      mse += out.error**2
    end
    
    back_propagate do |el|
      if el.is_a? Neuron
        neuron = el
        # Calculate error for neuron in hidden layer
        neuron.error = neuron.outputs.map{|o| o.weight*o.to.error}.inject(0,:+)*neuron.gradient
      elsif el.is_a? Connection
        con = el
        # Calculate next weight for connection
        con.weight_diff = NetworkSettings::MOMENTUM*con.weight_diff +
          NetworkSettings::LEARNING_RATE*con.to.error*con.from.value
        con.next_weight = con.weight + con.weight_diff
      end
    end
    
    # Finally, update the weights with their new values
    @connections.each do |c|
      c.weight = c.next_weight
      c.next_weight = nil
    end

    mse
  end
 
end

