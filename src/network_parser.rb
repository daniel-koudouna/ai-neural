require_relative './network.rb'

class NetworkParser
 
  def self.from_file(filename)
    self.from_hash JSON.parse(File.read(filename)) 
  end

  def self.to_file(network, filename)
    File.open(filename, "w") {|f| f.write(JSON.pretty_generate(to_hash(network))) }
  end
 
  def self.from_hash(obj)

    current_layer = []
    next_layer = []

    input_layer = []
    output_layer = []
    
    obj["nr_inputs"].times{|n| current_layer << InputNeuron.new }

    input_layer = current_layer.dup

    obj["layers"].each do |layer_obj|
      next_layer = []

      type = layer_obj["activation_function"]["type"]
      function = Function.get(type)
      raise "Unknown function type #{type}" if function == nil
      
      layer_obj["input_weights"].each do |weight_set|
        new_neuron = Neuron.new(function)
        next_layer << new_neuron

        current_layer.zip(weight_set).each do |neuron, weight|
          neuron.connect(new_neuron,weight)
        end
      end

      current_layer = next_layer
      
    end

    output_layer = current_layer.dup

    Network.new input_layer, output_layer
    
  end

  def self.to_hash(network)
    
    obj = {}
    obj["nr_inputs"] = network.inputs.size
    obj["nr_outputs"] = network.outputs.size
    obj["nr_layers"] = 1
    obj["layers"] = []

    all_neurons = network.inputs.dup
    last_layer = network.inputs.dup

    previous_layer = last_layer
    
    # While there are neurons to be accounted for
    while (all_neurons.size != network.neurons.size)
      previous_layer = last_layer
      last_layer = last_layer
      .map{|n| n.outputs}
      .flatten
      .map{|c| c.to}
      .uniq

      raise 'network is not feed forward' if last_layer.any?{|n| all_neurons.include? n}

      last_layer.each{|n| all_neurons << n}
      
      # if there is one output, then all should be outputs
      if (last_layer.any?{|n| network.outputs.include? n})
        raise 'network is not of the correct format' unless
          last_layer.all?{|n| network.outputs.include? n} && last_layer.size == network.outputs.size
      end

      current_function = nil
      
      layer_obj = {}
      layer_obj["nr_nodes"] = last_layer.size
      layer_obj["input_weights"] = []
      last_layer.each do |neuron|
        if (layer_obj["activation_function"] == nil)
          current_function = neuron.function
          layer_obj["activation_function"] = current_function.to_hash
        else
          raise "layer doesn't have the same activation function" unless
            neuron.function == current_function
        end

        weight_set =  neuron.inputs.map{|c| c.weight }

        layer_obj["input_weights"] << weight_set

        raise "network not fully connected (found #{weight_set.size} , expected #{previous_layer.size})" if weight_set.size != previous_layer.size
        
      end

      obj["layers"] << layer_obj
      obj["nr_layers"] = obj["nr_layers"] + 1
    end

    obj
  end
 
end
