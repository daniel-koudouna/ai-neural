require_relative './network_settings.rb'

class Game
  
  NONE = {symbol: " ", value: NetworkSettings::EMPTY_VALUE, alt_symbols: ["_","-"] }
  CROSS = {symbol: "X" , value: NetworkSettings::X_VALUE, alt_symbols: ["x"] }
  NAUGHT = {symbol: "O" , value: NetworkSettings::O_VALUE, alt_symbols: ["o"] }
    
  attr_accessor :board, :winner, :turn
  attr_accessor :x_moves, :o_moves
  attr_accessor :last_played
  
  EMPTY_LAYER = "   |   |   "
  MID_LAYER = "___|___|__"

  def self.board_from(string)

    arr = string.chars
    accepted = [NONE,CROSS,NAUGHT].map{|t| [t[:symbol] , t[:alt_symbols] ]}.flatten
      
    result = arr.select{|el| accepted.include? el}.map{|el| [NONE,CROSS,NAUGHT].select{|t| t[:symbol] == el || t[:alt_symbols].include?(el)}.first} 
    raise "Array contained invalid symbol or has invalid size" unless result.size == 9
    result
  end
  
  def initialize(board=nil)
    if (board.class == String)
      @board = Game.board_from(board)
    else
      @board = [NONE]*9
    end
    @x_moves = []
    @o_moves = []
  end

  def  layer(x,y,z)
    " #{@board[x][:symbol]} | #{@board[y][:symbol]} | #{@board[z][:symbol]} "
  end
  
  def draw
    puts EMPTY_LAYER
    puts layer(0,1,2)
    puts MID_LAYER
    puts EMPTY_LAYER
    puts layer(3,4,5)
    puts MID_LAYER
    puts EMPTY_LAYER
    puts layer(6,7,8)
  end

  def x(n)
    if @board[n] == NONE
      @x_moves.push( {board: to_training, played: n + 1})
      @board[n] = CROSS
      @last_played = n
      return true
    else
      return false
    end
  end

  def o(n)
    if @board[n] == NONE
      @o_moves.push( {board: to_training, played: n + 1})
      @board[n] = NAUGHT
      @last_played = n
      return true
    else
      return false
    end
  end

  def print_network_move
    puts "The network played on #{@last_played + 1}"
  end
  
  def get_best(outputs, f)
    outputs.each_with_index.sort{|a,b| b[0] <=> a[0]}.each do |x,n|
      if @board[n] == NONE
        f.call n
        return
      end
    end
  end

  def ai_move()
    arr = [
     ai_can_win(0,1,2), ai_can_win(3,4,5) , ai_can_win(6,7,8),
     ai_can_win(0,3,6), ai_can_win(1,3,7) , ai_can_win(2,4,8),
     ai_can_win(0,4,8), ai_can_win(2,4,6)
          ].keep_if{|t| t[0]}.map{|t| t[1]}

    if !arr.empty?
      o(arr.sample)
    else
      random_move(method(:o))
    end
    (@last_played + 1)
  end

  def ai_can_win(x,y,z)
    arr = [@board[x], @board[y], @board[z]]

    if arr.all?{|n| n != CROSS} && arr.one?{|n| n == NONE}
      return [true, (@board[x] == NONE) ? x : (@board[y] == NONE ? y : z)]
    else
      return [false, nil]
    end
    
  end
      
  def random_move(f)
    array = Array.new(9) {2*rand() - 1}
    array[4] += 0.6
    array[0] += 0.2
    array[2] += 0.2
    array[6] += 0.2
    array[8] += 0.2
    get_best(array,f)
  end
  
  def check(x,y,z)
    arr = [ @board[x] , @board[y] ,@board[z] ]
    if arr.all?{|n| n == CROSS }
      @winner = CROSS
      return true
    elsif arr.all?{|n| n == NAUGHT }
      @winner = NAUGHT
      return true
    else
      return false
    end
  end

  def check_tie
    if @board.all?{|n| n != NONE }
      @winner = NONE
      return true
    end 
  end
  
  def prompt
    print "Type a number between 1 and 9: "
  end

  def to_training
    @board.map{|n| n[:symbol]}
  end
  
  def to_network
    @board.map{|n| n[:value]}
  end

  def to_flipped_network
    @board.map{|n|
      if (n == NONE)
        NONE
      elsif (n == CROSS)
        NAUGHT
      else
        CROSS
      end
    }.map{|n| n[:value]}
  end
  
  def has_winner?   
    check(0,1,2) ||
      check(3,4,5) ||
      check(6,7,8) ||
      check(0,3,6) ||
      check(1,4,7) ||
      check(2,5,8) ||
      check(0,4,8) ||
      check(2,4,6) ||
      check_tie
  end
end
