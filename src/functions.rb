class Function

  @@types = {}

  def self.get(name)
    @@types[name]
  end
  
  def self.f(x)
    raise 'method not implemented'
  end

  def self.df(x)
    raise 'method not implemented'
  end

  def self.to_hash
    raise 'method not implemented'
  end
  
  def self.inherited(subclass)
    @@types[subclass.name.downcase] = subclass
  end
end

class Sigmoid < Function
  def self.f(x)
    1.0/(1.0 + Math.exp(-x))
  end

  def self.df(x)
    x*(1-x)
  end

  def self.to_hash
    {"type" => "sigmoid", "alpha" => "1"}
  end
end

class Tanh < Function
  def self.f(x)
    Math.tanh(x)
  end

  def self.df(x)
    1 - (Math.tanh(x))**2
  end

  def self.to_hash
    {"type" => "tanh" }
  end
end

class Step < Function
  def self.f(x)
    (x > 0.5 ? 1 : 0)
  end
  
  def self.df(x)
    0
  end

  def self.to_hash
    {"type" => "step" }
  end
end
