require_relative "./network_settings.rb"
require_relative "./game.rb"
require_relative './network.rb'
require 'json'

class NetworkTrainer
  def self.generate(filename, &block)
    data = NetworkTrainer.new

    data.instance_eval(&block)

    data.write_to filename
  end

  attr_accessor :examples

  def initialize
    @examples = []
  end

  def write_to(filename)
    obj = {epochs: NetworkSettings::EPOCHS, data: @examples}
    File.open(filename, "w") {|f| f.write(JSON.pretty_generate(obj)) }
  end

  #Train a good move
  def on(board_string, *positions)
    @examples << Training.from_string(board_string, *positions)
  end

  def flip(positions)
    positions.map{|p|
      case p
      when 0
        6
      when 1
        3
      when 2
        0
      when 3
        7
      when 4
        4
      when 5
        1
      when 6
        8
      when 7
        5
      when 8
        2
      end
    }
  end
  
  def permute_on(board_string, *positions)
    arr = board_string.gsub!(",","").chars
    raise "Bad format" unless arr.size == 9
    [
     [board_string, positions],
     [[
      arr[6],arr[3],arr[0],
      arr[7],arr[4],arr[1],
      arr[8],arr[5],arr[2]
     ].inject(:+), flip(positions)],
     [[
      arr[8],arr[7],arr[6],
      arr[5],arr[4],arr[3],
      arr[2],arr[1],arr[0]
     ].inject(:+), flip(flip(positions))],
     [[
      arr[2],arr[5],arr[8],
      arr[1],arr[4],arr[7],
      arr[0],arr[3],arr[6]
     ].inject(:+), flip(flip(flip(positions)))]
    ].each do |string, pos|
      puts string
      puts pos.inspect
      on(string,*pos)
    end
  end
  
  #Train a bad move
  def not_on(board_string, *positions)
    @examples << Training.bad_from_string(board_string, *positions)
  end
  
end

# Representation of the training data
class Training
  attr_accessor :epochs, :data

  def self.bad_from_string(string, *positions)
    state = Game.new(string)

    positions = positions.map{|p| p - 1}

    [state.to_network, 9.times.map{|n|
       (positions.include? n) ? NetworkSettings::SCOLD :
         (n == NetworkSettings::EMPTY_VALUE ? NetworkSettings::ENCOURAGE : NetworkSettings::NEUTRAL)}]
  end
  
  def self.from_string(string, *positions)

    state = Game.new(string)

    positions = positions.map{|p| p - 1}
    
    [state.to_network , 9.times.map{|n|
       (positions.include? n) ? NetworkSettings::ENCOURAGE :
         (n == NetworkSettings::EMPTY_VALUE ? NetworkSettings::NEUTRAL : NetworkSettings::DISCOURAGE)}
    ]
  end
  
  def self.from(filename)
    json = JSON.parse(File.read(filename))
    Training.new json["epochs"],json["data"]
  end

  def initialize(epochs, data)
    @epochs = epochs
    @data = data
  end

  # Trains the network on the set of training data
  def train(network)
    print ""
    @epochs.times do |n|
      total_err = 0
      @data.each do |input,expected|
        err = network.train input, expected
        total_err += err
      end
      printf "\rTraining: %-6s\toutput MSE: %-8s", "#{(100*n/(1.0*@epochs - 1)).round(1)}%" , "#{((0.0001+total_err)/(0.0001+@data.size)).round(5)}"
    end
    print "\n"
  end
  
end
