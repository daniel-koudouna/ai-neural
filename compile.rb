require_relative "./src/network_trainer.rb"

trainer = NetworkTrainer.new

ARGV.each do |file|

  next unless File.exists? file

  File.readlines(file).each do |line|
    trainer.instance_eval line
  end
end

trainer.write_to("data/training.json")
