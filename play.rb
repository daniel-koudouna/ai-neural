require_relative './src/neuron.rb'
require_relative './src/network.rb'
require_relative './src/network_parser.rb'
require_relative './src/network_trainer.rb'
require_relative './src/game.rb' 

if (ARGV[0] == nil)
  puts "You need to supply a filename to load the network"
  return
end

network = NetworkParser.from_file(ARGV[0])

EXIT_MESSAGE = "exit"
NEW_FIRST_MESSAGE = "0"
NEW_SECOND_MESSAGE = "1"
TRAIN_MESSAGE = "2"
SAVE_MESSAGE = "3"

BREAK = "--------------------"

puts "Neural Network Tic-Tac-Toe"

ARGV.clear

while(true)

  puts "You can exit at any time by typing '#{EXIT_MESSAGE}'"
  puts "Type '#{NEW_FIRST_MESSAGE}' for a new game going first"
  puts "Type '#{NEW_SECOND_MESSAGE}' for a new game going second"
  puts "Type '#{TRAIN_MESSAGE}' for the network to train by playing games"
  puts "Type '#{SAVE_MESSAGE}' to save the network to disk"
  print ">> "
  input = gets.chomp

  break unless input
  break if input == EXIT_MESSAGE


  if (SAVE_MESSAGE.include? input)

    name = "network-#{Time.now.to_i}.json"

    NetworkParser.to_file(network,name)

    puts "Saved as #{name}"
    
  elsif (TRAIN_MESSAGE.include? input)

    reps = 5
    
    sets = 1000
    reps.times do |r|

      winning_games = []
      drawn_games = []
      lost_games = []
      won = 0
      draw = 0
      
      
      sets.times do |n|
        game = Game.new
        
        network_turn = [true,false].sample
        
        while !(game.has_winner?)
        if (network_turn)
          output = network.accept game.to_network
          game.get_best(output, game.method(:x))
          network_turn = false
        else
          output = game.ai_move()
          network_turn = true
        end
        end
        
        if (game.winner == Game::CROSS)
          winning_games.push game.x_moves
          won+= 1
        elsif (game.winner == Game::NONE)
          draw += 1
          drawn_games.push game.x_moves
        else
          lost_games.push game.x_moves
        end
        
      end
      
      puts "Won #{won}, Lost #{sets - won - draw} , Draw #{draw}"
      
      
      trainer = NetworkTrainer.new
      strings = []
      
      winning_games.sample(winning_games.size*NetworkSettings::WIN_TRAIN_RATIO).each do |t|
        t.each do |h|
          played = h[:played]
          board = h[:board].inject{|a,b| a + b}
          str = "on \"#{board}\" , #{played}\n"
          strings << str
          trainer.instance_eval str
        end
      end

      drawn_games.sample(drawn_games.size*NetworkSettings::DRAWN_TRAIN_RATIO).each do |t|
        t.each do |h|
          played = h[:played]
          board = h[:board].inject{|a,b| a + b}
          str = "on \"#{board}\" , #{played}\n"
          strings << str
          trainer.instance_eval str
        end
      end
      
      lost_games.sample(lost_games.size*NetworkSettings::LOST_TRAIN_RATIO).each do |t|
        t.last(1).each do |h|
          board = h[:board].inject{|a,b| a + b}
          game = Game.new(board)

          block = game.ai_move()
          
          str = "on \"#{board}\", #{block}\n"
          strings << str
          trainer.instance_eval str
        end
      end

      File.open("data/training-data-#{Time.now.to_i}.rb", "w") {|f|
        strings.each{|s|
          f.write s
        }
      }
      
      trainer.write_to "data/temp.json"
      
      Training.from("data/temp.json").train network
    end

  elsif ([NEW_FIRST_MESSAGE,NEW_SECOND_MESSAGE].include? input)

    puts BREAK
    
    game = Game.new

    human_turn = (input == NEW_FIRST_MESSAGE)

    while (!game.has_winner?)
      game.draw
      if (human_turn)
        game.prompt
        input = gets.chomp
        break unless input
        break if input == EXIT_MESSAGE
        if (input == "x")
          game.random_move(game.method(:o))
          human_turn = false
        else
          n = input.to_i
          if (n > 0 && n < 10)
            human_turn = false if game.o(n - 1)
          end
        end
      else
        output = network.accept game.to_network
        puts "The network responded with #{output.inspect}\n"
        game.get_best(output,game.method(:x))
        game.print_network_move
        human_turn = true
      end
    end

    game.draw

    if (game.has_winner?)
      if (game.winner == Game::NAUGHT)
        puts "You win! Congratulations!"

        trainer = NetworkTrainer.new

        last_x = game.x_moves.last
        last_o = game.o_moves.last
        
        trainer.instance_eval "on \"#{last_x[:board].inject(:+)}\", #{last_o[:played]}"

        trainer.write_to "data/temp_game.json"
        Training.from("data/temp_game.json").train network
        
      elsif (game.winner == Game::CROSS)
        puts "The computer beat you! Try again next time :D"
      else
        puts "It's a tie!"
      end
    end
    
  end
end

