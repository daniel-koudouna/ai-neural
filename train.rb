require_relative './src/neuron.rb'
require_relative './src/network.rb'
require_relative './src/network_parser.rb'
require_relative './src/network_trainer.rb'
require_relative './src/game.rb' 

if (ARGV[0] == nil)
  puts "You need to supply a filename to load and save the trained network"
  return
end

if File.exists? ARGV[0]
  network = NetworkParser.from_file ARGV[0]
else
  network = Network.build 9, [30, Sigmoid] , [30, Sigmoid] , [9, Sigmoid]
end

ARGV[1] ||= 1

ARGV[1].to_i.times do |n|
  Training.from("data/training.json").train network
end
  

NetworkParser.to_file(network, ARGV[0])
